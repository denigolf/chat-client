import { combineReducers } from 'redux';
import chat from '../containers/Chat/reducer';
import userPage from '../containers/userPage/reducer';
import users from '../containers/users/reducer';
import login from '../containers/LoginPage/reducer';

const rootReducer = combineReducers({ chat, userPage, users, login });

export default rootReducer;
