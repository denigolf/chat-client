import { createReducer } from '@reduxjs/toolkit';
import {
  SET_CURRENT_USER_ID,
  DROP_CURRENT_USER_ID,
  SHOW_PAGE,
  HIDE_PAGE,
} from './actionTypes';

const initialState = {
  userId: '',
  isShown: false,
};

const userPageReducer = createReducer(initialState, (builder) => {
  builder.addCase(SET_CURRENT_USER_ID, (state, action) => {
    const { id } = action.payload;
    state.userId = id;
  });
  builder.addCase(DROP_CURRENT_USER_ID, (state) => {
    state.userId = '';
  });
  builder.addCase(SHOW_PAGE, (state) => {
    state.isShown = true;
  });
  builder.addCase(HIDE_PAGE, (state) => {
    state.isShown = false;
  });
});

export default userPageReducer;
