import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as actions from './actions';
import {
  updateUser,
  addUserMiddleware,
  editUserMiddleware,
} from '../users/actions';
import TextInput from '../../shared/components/inputs/text/TextInput';
import PasswordInput from '../../shared/components/inputs/password/PasswordInput';
import userFormConfig from '../../shared/config/userFormConfig';
import defaultUserConfig from '../../shared/config/defaultUserConfig';

const UserPage = () => {
  const getDefaultUserData = () => {
    return {
      ...defaultUserConfig,
    };
  };

  const [currentUser, setCurrentUser] = useState(getDefaultUserData());
  const isShown = useSelector((state) => state.userPage.isShown);
  const users = useSelector((state) => state.users);
  const userId = useSelector((state) => state.userPage.userId);
  const dispatch = useDispatch();

  useEffect(() => {
    const user = users.find((user) => user.id === userId);
    if (user) {
      setCurrentUser(user);
    }
  }, []);

  const onCancel = () => {
    dispatch(actions.dropCurrentUserId());
    dispatch(actions.hidePage());
    setCurrentUser(getDefaultUserData());
  };

  const onSave = () => {
    if (userId) {
      dispatch(editUserMiddleware(userId, currentUser));
      dispatch(updateUser(userId, currentUser));
    } else {
      dispatch(addUserMiddleware(currentUser));
    }
    dispatch(actions.dropCurrentUserId());
    dispatch(actions.hidePage());
    setCurrentUser(getDefaultUserData());
  };

  const onChangeData = (e, keyword) => {
    const value = e.target.value;
    setCurrentUser({ ...currentUser, [keyword]: value });
  };

  const getInput = (data, { label, type, keyword }) => {
    switch (type) {
      case 'text':
        return (
          <TextInput
            key={keyword}
            label={label}
            type={type}
            text={data[keyword]}
            keyword={keyword}
            onChange={onChangeData}
          />
        );
      case 'password':
        return (
          <PasswordInput
            key={keyword}
            label={label}
            type={type}
            text={data[keyword]}
            keyword={keyword}
            onChange={onChangeData}
          />
        );
      default:
        return null;
    }
  };

  const getUserPageContent = () => {
    return (
      <div
        className="modal"
        style={{ display: 'block' }}
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content" style={{ padding: '5px' }}>
            <div className="modal-header">
              <h5 className="modal-title">Add user</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={onCancel}
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {userFormConfig.map((item) => getInput(currentUser, item))}
            </div>
            <div className="modal-footer">
              <button className="btn btn-secondary" onClick={onCancel}>
                Cancel
              </button>
              <button className="btn btn-primary" onClick={onSave}>
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  };

  return isShown ? getUserPageContent() : null;
};

export default UserPage;
