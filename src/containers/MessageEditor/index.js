import React, { useState, useEffect } from 'react';
import {
  toggleEditModal,
  dropCurrentMessageId,
  editMessageMiddleware,
} from '../Chat/actions';
import { useDispatch, useSelector } from 'react-redux';
import './EditModal.scss';
import { Redirect } from 'react-router-dom';

const MessageEditor = () => {
  const dispatch = useDispatch();
  const [inputValue, setInputValue] = useState('');
  const messages = useSelector((state) => state.chat.messages);
  const editModal = useSelector((state) => state.chat.editModal);
  const currentMessageId = useSelector((state) => state.chat.currentMessageId);

  useEffect(() => {
    if (currentMessageId) {
      const currentMessage = messages.find(
        (message) => message.id === currentMessageId
      );
      setInputValue(currentMessage.text);
    }
  }, [currentMessageId, messages]);

  const onSave = () => {
    dispatch(
      editMessageMiddleware(currentMessageId, inputValue, setInputValue)
    );
  };

  const onClose = () => {
    dispatch(dropCurrentMessageId());
    dispatch(toggleEditModal());
  };

  return (
    <div className="edit-message-modal modal-shown">
      <div className="edit-message-modal__container">
        <span className="edit-message-modal__title">Edit your message:</span>
        <input
          className="edit-message-input edit-message-modal__input"
          type="text"
          onChange={(e) => setInputValue(e.target.value)}
          value={inputValue}
          autoFocus
        />
        <div className="edit-message-modal__buttons">
          <button className="edit-message-close" onClick={onClose}>
            Close
          </button>

          <button className="edit-message-button" onClick={onSave}>
            Save
          </button>
        </div>
      </div>
      {editModal ? <Redirect to="/edit" /> : <Redirect to="/chat" />}
    </div>
  );
};

export default MessageEditor;
