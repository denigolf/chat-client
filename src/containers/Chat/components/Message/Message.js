import React, { useState } from 'react';
import { formatDateMessage } from '../../../../shared/helpers';
import './Message.scss';

const Message = ({ message }) => {
  const [isLiked, setIsLiked] = useState(false);

  const likeHandler = () => {
    setIsLiked(!isLiked);
  };

  return (
    <div className="message">
      <img
        className="message-user-avatar"
        src={message.avatar}
        alt={message.user}
      />
      <div className="message-text-container">
        <span className="message-user-name">{message.user}</span>

        <div className="message-text-button">
          <span className="message-text">{message.text}</span>
          <button
            className={isLiked ? 'message-liked' : 'message-like'}
            onClick={likeHandler}
          >
            &#x2764;
          </button>
        </div>
        <span className="message-time">
          {formatDateMessage(message.createdAt)}
        </span>
      </div>
    </div>
  );
};

export default Message;
