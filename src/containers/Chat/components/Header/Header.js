import React from 'react';
import { formatDateHeader } from '../../../../shared/helpers';
import './Header.scss';

const Header = ({ messages }) => {
  const getParticipantsCount = () => {
    const users = [];
    messages.forEach((message) => {
      const userId = message.userId;
      if (!users.includes(userId)) {
        users.push(userId);
      }
    });

    return users.length;
  };

  const getLastMessageTime = () => {
    const lastMessage = messages[messages.length - 1];
    const lastMessageTime = lastMessage.createdAt;
    return lastMessageTime;
  };

  const getMessagesCount = () => {
    return messages.length;
  };

  return (
    <header className="header">
      <div className="header-info">
        <div className="header-title">Ozark</div>
        <div className="header-users-count-container">
          <span className="header-users-count">{getParticipantsCount()}</span>
          <span> participants</span>
        </div>
        <div className="header-messages-count-container">
          <span className="header-messages-count">{getMessagesCount()}</span>
          <span>{getMessagesCount() === 1 ? ' message' : ' messages'}</span>
        </div>
      </div>
      <div className="header-last-message-date-container">
        <span>last message </span>
        <span className="header-last-message-date">
          {formatDateHeader(getLastMessageTime())}
        </span>
      </div>
    </header>
  );
};

export default Header;
