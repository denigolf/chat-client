import React from 'react';
import { formatDateMessage } from '../../../../shared/helpers';
import {
  toggleEditModal,
  setCurrentMessageId,
  deleteMessageMiddleware,
} from '../../actions';
import { useDispatch } from 'react-redux';
import './OwnMessage.scss';

const OwnMessage = ({ message }) => {
  const dispatch = useDispatch();

  const editMessage = () => {
    dispatch(setCurrentMessageId(message.id));
    dispatch(toggleEditModal());
  };

  const deleteMessageHandler = () => {
    dispatch(deleteMessageMiddleware(message.id));
  };

  return (
    <div className="own-message">
      <div className="message-text-container">
        <span className="message-text">{message.text}</span>
        <span className="message-time">
          {formatDateMessage(message.createdAt)}
        </span>
      </div>
      <button className="message-edit" onClick={editMessage}>
        &#9998;
      </button>
      <button className="message-delete" onClick={deleteMessageHandler}>
        &#10006;
      </button>
    </div>
  );
};

export default OwnMessage;
