import React, { useState } from 'react';
import { createMessageMiddleware } from '../../actions';
import { useDispatch } from 'react-redux';
import './MessageInput.scss';

const MessageInput = () => {
  const [inputValue, setInputValue] = useState('');
  const dispatch = useDispatch();

  const changeInputHandler = (e) => {
    const inputValue = e.target.value;
    setInputValue(inputValue);
  };

  const clearInput = () => {
    setInputValue('');
  };

  const sendMessageHandler = () => {
    dispatch(createMessageMiddleware(inputValue, clearInput));
    clearInput();
  };

  return (
    <div className="message-input">
      <input
        className="message-input-text"
        type="text"
        placeholder="Enter your message..."
        onChange={changeInputHandler}
        value={inputValue}
      />
      <button className="message-input-button" onClick={sendMessageHandler}>
        Send
      </button>
    </div>
  );
};

export default MessageInput;
