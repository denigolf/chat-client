import { ADD_USER, UPDATE_USER, DELETE_USER, LOAD_USERS } from './actionTypes';

const initialState = [];

const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_USERS: {
      const { data } = action.payload;
      return [...data];
    }
    case ADD_USER: {
      const { data } = action.payload;
      const newUser = { ...data };
      return [...state, newUser];
    }

    case UPDATE_USER: {
      const { id, data } = action.payload;
      const updatedUsers = state.map((user) => {
        if (user.id === id) {
          return {
            ...user,
            ...data,
          };
        } else {
          return user;
        }
      });

      return updatedUsers;
    }

    case DELETE_USER: {
      const { id } = action.payload;
      const filteredUsers = state.filter((user) => user.id !== id);
      return filteredUsers;
    }

    default:
      return state;
  }
};

export default usersReducer;
