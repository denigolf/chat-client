import { LOGIN_SUCCESS, LOGIN_ERROR, LOGIN_ADMIN } from './actionTypes';

export const loginSuccess = () => ({
  type: LOGIN_SUCCESS,
});

export const loginError = (message) => ({
  type: LOGIN_ERROR,
  payload: {
    message,
  },
});

export const loginAdmin = () => ({
  type: LOGIN_ADMIN,
});

export const userLoginMiddleware = (loginData) => {
  return async (dispatch, store) => {
    try {
      const { login, password } = loginData;
      const response = await fetch(
        `https://serene-eyrie-26293.herokuapp.com/api/users/${login}`
      );
      const data = await response.json();

      if (data.error) {
        dispatch(loginError("User with this login doesn't exist!"));
        return;
      }
      if (data.password !== password) {
        dispatch(loginError('Password is not correct!'));
        return;
      }
      if (login === 'admin' && password === 'admin') {
        dispatch(loginAdmin());
      }
      if (data) {
        dispatch(loginSuccess());
      }
    } catch (err) {
      dispatch(loginError(err.message));
    }
  };
};
