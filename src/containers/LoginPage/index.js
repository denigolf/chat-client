import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router';
import { userLoginMiddleware } from './actions';

const LoginPage = () => {
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');
  const isAuthed = useSelector((state) => state.login.isAuthed);
  const isError = useSelector((state) => state.login.isError);
  const errorMessage = useSelector((state) => state.login.errorMessage);
  const dispatch = useDispatch();

  const loginHandler = (e) => {
    e.preventDefault();
    dispatch(userLoginMiddleware({ login, password }));
  };

  return (
    <form
      className="d-flex flex-column justify-content-center min-vh-100"
      onSubmit={loginHandler}
    >
      {isAuthed ? <Redirect to="/chat" /> : null}
      {isError ? (
        <div className="alert alert-danger" role="alert">
          {errorMessage}
        </div>
      ) : null}

      <div className="mb-3">
        <label htmlFor="exampleInputEmail1" className="form-label">
          Login
        </label>
        <input
          type="text"
          className="form-control"
          onChange={(e) => setLogin(e.target.value)}
        />
      </div>
      <div className="mb-3">
        <label htmlFor="exampleInputPassword1" className="form-label">
          Password
        </label>
        <input
          type="password"
          className="form-control"
          id="exampleInputPassword1"
          onChange={(e) => setPassword(e.target.value)}
          required
        />
      </div>
      <button type="submit" className="btn btn-primary">
        Submit
      </button>
    </form>
  );
};

export default LoginPage;
