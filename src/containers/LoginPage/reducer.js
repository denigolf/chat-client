import { LOGIN_SUCCESS, LOGIN_ERROR, LOGIN_ADMIN } from './actionTypes';
import { createReducer } from '@reduxjs/toolkit';

const initialState = {
  isAuthed: false,
  isError: false,
  errorMessage: '',
  isAdmin: false,
};

const loginReducer = createReducer(initialState, (builder) => {
  builder.addCase(LOGIN_SUCCESS, (state) => {
    state.isAuthed = true;
    state.isError = false;
    state.errorMessage = '';
  });
  builder.addCase(LOGIN_ERROR, (state, action) => {
    const { message } = action.payload;
    state.isError = true;
    state.errorMessage = message;
  });
  builder.addCase(LOGIN_ADMIN, (state, action) => {
    state.isAuthed = true;
    state.isError = false;
    state.errorMessage = '';
    state.isAdmin = true;
  });
});

export default loginReducer;
