import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

const Navigation = () => {
  const isAdmin = useSelector((state) => state.login.isAdmin);

  return (
    <nav
      className="navbar navbar-expand-lg navbar-dark w-100"
      style={{ backgroundColor: '#39658a', padding: '20px' }}
    >
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item active">
            <Link to="/chat" className="nav-link">
              Chat
            </Link>
          </li>
          {isAdmin ? (
            <li className="nav-item">
              <Link to="/user" className="nav-link">
                User List
              </Link>
            </li>
          ) : null}
        </ul>
      </div>
    </nav>
  );
};

export default Navigation;
